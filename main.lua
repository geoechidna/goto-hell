function love.load()
    love.graphics.setDefaultFilter("nearest")
    require "code/requires"    --add all required files
    screenX = love.window.getWidth()
    screenY = love.window.getHeight()
    state.mainMenu()
end

function love.update(dt)
    if dt > 1/15 then
        dt = 1/15
    end
    state.update(dt)
end

function love.draw()
    state.draw()
end

function love.keypressed(key)
    -- If escape is pressed end the game
    if key == "escape" then
        if state.current == "menu" then
            love.event.quit()       -- End the game
        else
            state.mainMenu()
        end
    end
end
