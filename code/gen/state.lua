state = { }

img = {
    menu = love.graphics.newImage("res/img/mainMenu.png"),
    buttonInactive = love.graphics.newImage("res/img/menuBarSleeping.png"),
    buttonActive = love.graphics.newImage("res/img/menuBarStunned.png"),
    buttonClicked = love.graphics.newImage("res/img/menuBarShocked.png"),
    title = love.graphics.newImage("res/img/titleBox.png"),
    gameBG = love.graphics.newImage("res/img/grassyPlaceWithSkyAndCloudsAndStuff.png")
}

love.graphics.setFont(love.graphics.newFont(75))

function state.mainMenu()
    state.current = "menu"
    state.bg = img.menu
    menu = {
        titleBox = {x = 0,
                    y = 0,
                    image = img.title},
        playButton={x = 70,
                    y = 240,
                    image = img.buttonInactive},
        optionsBtn={x = 70,
                    y = 380,
                    image = img.buttonInactive},
        quitButton={x = 70,
                    y = 520,
                    image = img.buttonInactive}
    }


end

function state.options()
end

function state.game(difficulty)
    state.current = "game"
    state.bg = img.gameBG
    player.init(difficulty or "insane")
end

function state.stats()
end

function state.gameOver()
    love.event.quit()
end

function state.update(dt)
    if state.current == "menu" then
        local x, y = love.mouse.getPosition()
        if x >= 70 and x <= 390 then
            if y >=240 and y <=340 then
                if love.mouse.isDown("l") then
                    menu.playButton.image = img.buttonClicked
                    menu.playButton.primed = true
                elseif menu.playButton.primed == true then
                    state.game("easy")
                else
                    menu.playButton.image = img.buttonActive
                end
            elseif y >= 380 and y <= 480 then
                if love.mouse.isDown("l") then
                    menu.optionsBtn.image = img.buttonClicked
                    menu.optionsBtn.primed = true
                elseif menu.optionsBtn.primed == true then
                    state.game()
                else
                    menu.optionsBtn.image = img.buttonActive
                end
            elseif y >= 520 and y <=620 then
                if love.mouse.isDown("l") then
                    menu.quitButton.image = img.buttonClicked
                    menu.quitButton.primed = true
                elseif menu.quitButton.primed == true then
                    love.event.quit()
                else
                    menu.quitButton.image = img.buttonActive
                end
            end
            if not (y >=240 and y <=340) then
                menu.playButton.image = img.buttonInactive
                menu.playButton.primed = false
            end
            if not (y >=380 and y <= 480) then
                menu.optionsBtn.image = img.buttonInactive
                menu.optionsBtn.primed = false
            end
            if not (y >=520 and y <=620) then
                menu.quitButton.image = img.buttonInactive
                menu.quitButton.primed = false
            end
        else
            menu.playButton.image = img.buttonInactive
            menu.optionsBtn.image = img.buttonInactive
            menu.quitButton.image = img.buttonInactive
        end
    end
    if state.current == "game" then
        player.update(dt)
        bullet.update(dt)
        debug.update(dt)
    end
end

function state.draw()
    d = love.graphics.draw
    d(state.bg, 0, 0)
    if state.current == "menu" then
        d(menu.titleBox.image, menu.titleBox.x, menu.titleBox.y)
        d(menu.playButton.image, menu.playButton.x, menu.playButton.y)
        d(menu.optionsBtn.image, menu.optionsBtn.x, menu.optionsBtn.y)
        d(menu.quitButton.image, menu.quitButton.x, menu.quitButton.y)
        love.graphics.print("Play", 100, 250)
        love.graphics.print("Options", 100, 390)
        love.graphics.print("Quit", 100, 530)
    end
    if state.current == "game" then
        player.draw(vx, vy)
        bullet.draw(vx, vy)
    end
end
