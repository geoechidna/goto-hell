-- Table containing all bullets
bullets = { }

-- Table containing functions
bullet = { }

-- Function to add new bullet
function bullet.add(bType, owner, x, y, targetType, targetVal, angleMod, speed)
    angleMod = angleMod or 0
    bullets[#bullets+1] = {
        bType = bType,
        owner = owner,
        x = x,
        y = y,
        targetType = targetType or "object",
        targetVal = targetVal or "player",
        speed = speed or 856,
        image = love.graphics.newImage("res/img/bullets/blue.png")
    }
    b = bullets[#bullets]
    if targetType == "object" then
        if targetVal == "player" then
            b.angle = misc.findPlayerDirection(x,y) + angleMod
        elseif targetVal == "playerWithSpread" then
            bullet.add(bType, owner, x, y, targetType, "player", angleMod)
            bullet.add(bType, owner, x, y, targetType, "player", -angleMod)
            b.angle = misc.findPlayerDirection(x,y)
        end
    elseif targetType == "direction" then
        if targetVal == "up" then
            b.angle = math.rad(270)
        elseif targetVal == "down" then
            b.angle = math.rad(90)
        elseif targetVal == "left" then
            b.angle = math.rad(180)
        else -- if targetVal == "right" then
            b.angle = math.rad(0)
        end
    elseif targetType == "angle" then -- REMEMBER!! Angles start from right and go clockwise. I.E. 90 is down
        b.angle = targetVal + angleMod
    end
    b.vX = math.cos(b.angle) * b.speed
    b.vY = math.sin(b.angle) * b.speed
end

-- Performs functions to move and update bullets
function bullet.update(dt)
	local toRemove = { }
    for key, object in ipairs(bullets) do
        object.x = object.x + object.vX *  dt
        object.y = object.y + object.vY *  dt
        if bullet.collision(object) then
        	table.insert(toRemove, key)
        end
    end
    for i = #toRemove, 1, -1 do
    	table.remove(bullets, toRemove[i])
    end
end

--checks collision of passed bullet with target
function bullet.collision(bullet)
    if bullet.owner == "player" then
		for key, enemy in ipairs(enemies) do
			if 	bullet.y + bullet.size > enemy.y
			and bullet.y < enemy.y + enemy.size
			and bullet.x + bullet.size > enemy.x
			and bullet.x < enemy.x + enemy.size then
				enemy.hit(key, bullet.bType)
				return true
			end
		end
	elseif owner == "enemy" then
			if 	bullet.y + bullet.size > player.y - 1
			and bullet.y < player.y + 1
			and bullet.x + bullet.size > player.x - 1
			and bullet.x < player.x + 1 then
				player.hit()
				return true
			end
        end

        return bullet.x < 0 or bullet.x > love.window.getWidth() or
               bullet.y < 0 or bullet.y > love.window.getHeight()

end

function bullet.draw()
    for key, b in ipairs(bullets) do
	    local x, y, sc
        x = b.x -- (math.floor(b.image:getHeight()/2))
        y = b.y -- (math.floor(b.image:getWidth()/2))
        if b.owner == "enemy" then
            sc = 2
        else sc = 1
        end
        love.graphics.draw(b.image, x, y, b.angle, sc)
    end
end
