misc = { }

function misc.findPlayerDirection(x,y)
	local pX, pY = player.x, player.y
	local dX = pX - x
	local dY = pY - y
	local angle = math.tan(dY/dX)
	return angle
end
