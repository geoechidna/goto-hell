-- Table containing all enemies
enemies = { }

-- Table containing functions
enemy = { }

sprite.enemy1 = love.graphics.newImage("res/img/enemy1.png")
sprite.enemy2 = love.graphics.newImage("res/img/enemy2.png")
-- Function to add new bullet
function enemy.add(typ, tx, ty, tpath, targetType, targetVal, angleMod)
-- path will be table with format
--  duration(seconds), xDirection, yDirection, speed
--  for each movement
    enemies[#enemies+1] = {
        x = tx,
        y = ty,
        eType = typ,
        timer = 0,
        path = tpath,
        steps = #path - #path % 4,
        currentStep = 0,
        currentTime = path[1],
        currentVx = path[2],
        currentVy = path[3],
        currentSpeed = path[4],
        targetType = targetType,
        taretVal = targetVal,
        angleMod = angleMod
    }
    local e = enemies[#enemies]
    if e.eType == 1 then
        e.image = sprite.enemy1
        e.health = 100
    else
        e.image = sprite.enemy2
        e.health = 200
    end
end

function enemy.update(dt)
    for key, enemy in ipairs(enemies) do
        enemy.timer = enemy.timer + dt
        if enemy.health < 0 then
            if not enemy.dTime then
                enemy.timer = 0
                enemy.dTime = true
            end
            enemy.image = res.img.hit


        else
            if enemy.timer >= enemy.currentTime then
                enemy.timer = 0
                enemy.currentStep = enemy.currentStep + 1
                enemy.currentTime = path[4 * enemy.currentStep + 1]
                enemy.currentVx = path[4 * enemy.currentStep + 2]
                enemy.currentVy = path[4 * enemy.currentStep + 3]
                enemy.currentSpeed = path[4 * enemy.currentStep + 4]
            end
            enemy.x = enemy.currentVx * enemy.currentSpeed * dt
            enemy.y = enemy.currentVy * enemy.currentSpeed * dt
            enemy.fire(enemy.targetType, enemy.targetVal, enemy.angleMod)
        end
    end

end
function enemy.draw()
    local e = enemies[key]
    for key, obj in ipairs(enemies) do
        love.graphics.draw(e.image, e.x, e.y)
    end
end

function enemy.fire(targetType, targetVal, angleMod)
    local e = enemies[key]
    bullet.add(1, "enemy", e.x, e.y, targetType, targetVal, angleMod, 384)
end


function enemy.hit(key, str)
  local e = enemies[key]
  e.health = e.health - 20 * str
end
