--[[
	Initialisation for Player
  ]]--

player = {
	score = 0
}

sprite = {
    ship = love.graphics.newImage("res/img/ship.png"),
    hit = love.graphics.newImage("res/img/hit.png"),
    blank = love.graphics.newImage("res/img/blank.png")
}

function player.init(diff)
	player.x = screenX / 2                     -- start player in lower middle of screen
	player.y = (9 / 10) * screenY
	player.vX = 0	         	               --X velocity
	player.vY = 0                              --Y velocity
	player.speed = 256			               --normal speed
	player.slowSpeed = 64		               --speed if 'slow' button is pressed
	player.shotCdCounter = 0                   --counts from 0 to shotCooldown, before shooting again
	player.shotCooldown = 0.2                  --fire delay for shots in seconds
	player.upgrade = 3                         --upgrade level
	if diff ~= "easy" and
            diff ~= "med" and
            diff ~= "hard" and
            diff ~= "insane" then
        diff = "easy"
    end
    if diff == "easy" then                       --set difficulty
		player.lives = 9
		player.bombs = 9
	elseif diff == "med" then
		player.lives = 5
		player.bombs = 5
	elseif diff == "hard" then
		player.lives = 3
		player.bombs = 1
	elseif diff == "insane" then
		player.lives = 1
		player.bombs = 0
	end
	player.fatalTime = 0.5                     --amount of time in seconds for emergency bomb
	player.image = sprite.ship
	player.scale = 2
	player.isHit = false
	player.respawnTimer = 2
	player.isInvincible = false
	player.invincTimer = 5
	player.timer = 0
	player.toBeRespawned = false
end

function player.update(dt)
	--if hit
	if player.isHit then
		player.image = sprite.hit
		player.timer = player.timer + dt
		if player.timer > player.fatalTime then            --give player final chance
			player.image = sprite.blank
		end
		if player.timer > player.respawnTimer and player.toBeRespawned then
			player.lives = player.lives - 1
            if player.lives < 1 then
                state.gameOver()
                return
            end
            player.toBeRespawned = false                 --reset player if ready
			player.x = screenX / 2
			player.y = 9 * screenY / 10
			player.image = sprite.ship
			player.shotCdCounter = 0
			player.upgrade = player.upgrade - 1
		end
		if player.timer > player.respawnTimer then
			player.isInvincible = false
			player.isHit = false
		end
    else


        --Movement control
        player.vX, player.vY = 0, 0
        if love.keyboard.isDown("left") and love.keyboard.isDown("right") then
            player.vX = 0
        elseif love.keyboard.isDown("left") then
            player.vX = -1
        elseif love.keyboard.isDown("right") then
            player.vX = 1
        end
        if love.keyboard.isDown("up") and love.keyboard.isDown("down") then
            player.vY = 0
        elseif love.keyboard.isDown("up") then
            player.vY = -1
        elseif love.keyboard.isDown("down") then
            player.vY = 1
        end
        slow = love.keyboard.isDown("lshift") or love.keyboard.isDown("rshift")
        if not (vX == 0 and vY == 0) then
            player.move(dt, slow)
        end
        --Fire
        local canShoot = false
        if player.shotCdCounter < player.shotCooldown then
            player.shotCdCounter = player.shotCdCounter + dt
        else
            canShoot = true
        end
        if love.keyboard.isDown(" ") and canShoot then
            player.fire()
            player.shotCdCounter = 0
        end
    end
end

function player.draw()
	local x, y
	x = player.x - (math.floor(player.image:getHeight()/2))
	y = player.y - (math.floor(player.image:getWidth()/2))
	love.graphics.draw(player.image, x, y, 0, player.scale)
end

function player.move(dt, slow)
	--select high speed or low speed
	local speed
	if slow then
		speed = player.slowSpeed
	else
		speed = player.speed
	end

    -- don't go offscreen
    if player.x + player.image:getWidth() /2 >= screenX and player.vX == 1 then
        player.vX = 0
    elseif player.x + player.image:getWidth() /2 <= 0 and player.vX == -1 then
        player.vX = 0
    end
    if player.y + player.image:getWidth() /2 >= screenY and player.vY == 1 then
        player.vY = 0
    elseif player.y + player.image:getWidth() /2 <= 0 and player.vY == -1 then
        player.vY = 0
    end



	--if N E S or W, then move full speed
	--else move diagonally at 1/sqrt2 speed each way,
	-- 	giving actual correct speed
	if player.vX == 0 or player.vY == 0 then
		if player.vX ~= 0 then
			player.x = player.x + player.vX * speed * dt
		else
			player.y = player.y + player.vY * speed * dt
		end
	else
		speed = speed / math.sqrt(2)
		player.x = player.x + player.vX * speed * dt
		player.y = player.y + player.vY * speed * dt
	end
end

function player.fire(upgrade)
                --generate bullets depending on updrade level
	local sc = player.scale
    local x = player.x + player.image:getWidth()/2
    local y = player.y + player.image:getHeight()/2
    if player.upgrade == 3 then
      --bullet.add(bType, owner, x, y, targetType, targetVal, angleMod)
		bullet.add(2, "player", x- 2*sc, y-11*sc, "angle", math.rad(255))
		bullet.add(2, "player", x+ 1*sc, y-11*sc, "angle", math.rad(285))
		bullet.add(2, "player", x- 4*sc, y- 6*sc, "direction", "up")
		bullet.add(2, "player", x+ 3*sc, y- 6*sc, "direction", "up")
		bullet.add(2, "player", x- 7*sc, y- 6*sc, "direction", "up")
		bullet.add(2, "player", x+ 6*sc, y- 6*sc, "direction", "up")
		bullet.add(2, "player", x-10*sc, y- 6*sc, "direction", "up")
		bullet.add(2, "player", x+ 9*sc, y- 6*sc, "direction", "up")
		bullet.add(2, "player", x-13*sc, y- 1*sc, "direction", "left")
		bullet.add(2, "player", x+12*sc, y- 1*sc, "direction", "right")
		bullet.add(2, "player", x-15*sc, y+ 8*sc, "direction", "up")
		bullet.add(2, "player", x+14*sc, y+ 8*sc, "direction", "up")
		bullet.add(2, "player", x-11*sc, y+ 5*sc, "angle", math.rad(240))
		bullet.add(2, "player", x+10*sc, y+ 5*sc, "angle", math.rad(300))
	else
        bullet.add(1, "player", x-(4*sc), y-6*sc, "direction", "up")
        bullet.add(1, "player", x+(3*sc), y-6*sc, "direction", "up")
        if player.upgrade == 1 or player.upgrade == 2 then
            bullet.add(1, "player", x-11*sc, y+5*sc, "angle", math.rad(255))
            bullet.add(1, "player", x+10*sc, y+5*sc, "angle", math.rad(285))
            if player.upgrade == 2 then
                bullet.add(1, "player", x-7*sc, y-sc, "direction", "up")
                bullet.add(1, "player", x+6*sc, y-sc, "direction", "up")
            end
        end
    end
end

function player.hit()
	if not player.isInvincible then
		player.timer = 0
		player.isHit = true
		player.toBeRespawned = true
	end
end
